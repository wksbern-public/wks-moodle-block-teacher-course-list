<?php
// This file is part of the blocks/wks_teacher_course_list Moodle plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * class block_wks_teacher_course_list
 *
 * @package    block_wks_teacher_course_list
 * @copyright  2018-2019 Liip AG {@link https://liip.ch}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot . '/course/lib.php');
// @fix Fabian
//require_once($CFG->libdir . '/coursecatlib.php');



class block_wks_teacher_course_list extends block_list {

    private $noneditingteacherroleid;

    /**
     * Array of category ids that are visible to the teacher
     */
    private $visiblecategoryids = [];

    public function init() {
        global $DB;

        $this->title = get_string('title', 'block_wks_teacher_course_list');

        $this->noneditingteacherroleid = null;
        $noneditingteacherrole = $DB->get_record('role', ['shortname' => 'teacher']);
        if ($noneditingteacherrole) {
            $this->noneditingteacherroleid = $noneditingteacherrole->id;
        }

        // Get the configuration on visible categories.
        $visiblecategoryconfig = get_config('block_wks_teacher_course_list', 'visiblecategories');
        if ($visiblecategoryconfig) {
            $this->visiblecategoryids = array_map(
                'intval',
                explode(',', $visiblecategoryconfig)
            );
        }
    }

    public function has_config() {
        return true;
    }

    /**
     * Filter for visible categories
     *
     * @param int categoryid
     * @return bool visibility of the category
     */
    private function is_visible_category($categoryid) {
        return in_array($categoryid, $this->visiblecategoryids);
    }

    public function get_content() {

        global $USER, $DB;

        if ($this->content !== null) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->items = [];
        $this->content->icons = [];
        $this->content->footer = '';

        // This is mostly taken from  admin/roles/userroles.php.
        // Get the role assignments for this user.
        $sql = "SELECT ra.id, ra.userid, ra.contextid, ra.roleid, ra.component, ra.itemid, c.path, c.contextlevel
            FROM {role_assignments} ra
            JOIN {context} c ON ra.contextid = c.id
            JOIN {role} r ON ra.roleid = r.id
            WHERE ra.userid = ?
            ORDER BY contextlevel DESC, contextid ASC, r.sortorder ASC";
        $roleassignments = $DB->get_records_sql($sql, [$USER->id]);

        // SUP-7524 Moved cat IDs to block settings.
        $visiblecategoriescontext = [];

        foreach ($this->visiblecategoryids as $catid) {
            // SUP-7377 Look up contexts for categories we want to include in the tree.
            $visiblecategoriescontext[$catid] = context_coursecat::instance($catid, IGNORE_MISSING);
        }

        // In order to display a nice tree of contexts, we need to get all the
        // ancestors of all the contexts in the query we just did.
        $requiredcontexts = [];
        foreach ($roleassignments as $ra) {
            $context = context::instance_by_id($ra->contextid);
            // Only consider the contexts where the user can edit a course.
            // Or user is non-editing teacher in that course (SUP-7524).
            if (has_capability('moodle/course:update', $context) || $this->is_non_editing_teacher($context)) {

                // SUP-7524 Keep only courses that belong to the Categorie from the block settings.
                foreach ($this->visiblecategoryids as $catid) {
                    if ($visiblecategoriescontext[$catid] && strpos($ra->path, $visiblecategoriescontext[$catid]->path) === 0) {
                        $requiredcontexts = array_merge($requiredcontexts, explode('/', trim($ra->path, '/')));
                    }
                }
            }
        }
        $requiredcontexts = array_unique($requiredcontexts);

        // Now load those contexts.
        if ($requiredcontexts) {
            list($sqlcontexttest, $contextparams) = $DB->get_in_or_equal($requiredcontexts);
            $contexts = get_sorted_contexts('ctx.id ' . $sqlcontexttest, $contextparams);
        } else {
            $contexts = [];
        }

        // Prepare some empty arrays to hold the data we are about to compute.
        foreach ($contexts as $conid => $con) {
            $contexts[$conid]->children = [];
            $contexts[$conid]->roleassignments = [];
        }

        // Put the contexts into a tree structure.
        foreach ($contexts as $conid => $con) {
            $context = context::instance_by_id($conid);
            $parentcontext = $context->get_parent_context();
            if ($parentcontext) {
                $contexts[$parentcontext->id]->children[] = $conid;
            }
        }

        // Put the role capabilities into the context tree.
        foreach ($roleassignments as $ra) {
            //echo var_dump($contexts[$ra->contextid]);
            // @fix by Fabian
            if(isset($contexts[$ra->contextid])) {
                $contexts[$ra->contextid]->roleassignments[$ra->roleid] = $ra;
            }


        }
        $systemcontext = context_system::instance();
        $this->build_tree($systemcontext->id, $contexts, $systemcontext);

        // Display the block only if a there is some tree to show.
        // We compare to one since the top category is in the tree.
        if (count($this->content->items) == 1) {
            $this->content = '';
        }
        return $this->content;
    }

    /**
     * Returns the role that best describes the course list block.
     *
     * @return string
     */
    public function get_aria_role() {
        return 'navigation';
    }

    /**
     * Build the (recursive) list of courses
     *
     * This populates $this->content->items[]
     *
     * $param int $contextid the current context id
     * $param array $contexts array of contexts
     * $param StdObj $systemcontext system context object
     * @return void
     */
    protected function build_tree($contextid, $contexts, $systemcontext, $level = 0) {
        global $OUTPUT;

        // Pull the current context into an array for convenience.
        $context = context::instance_by_id($contextid);

        // Rather ugly hack to be able to use the block_list class.
        // Otherwise the html code would have to be build manually.
        $indent = "";
        for ($i = 0; $i <= $level; $i++) {
            $indent .= "&nbsp;";
        }
        // Print the context name.
        // Add a link if the user has update/edit rights in a category/course.
        $link = false;
        if ($context->contextlevel == CONTEXT_COURSECAT &&
            has_capability('moodle/course:create', $context)) {
            $link = true;
            $icon = "";
        } else if (has_capability('moodle/course:update', $context) || $this->is_non_editing_teacher($context)) {
            $link = true;
            $icon  = $OUTPUT->pix_icon('i/course', new lang_string('course'));
        }

        if ($link) {
            $this->content->items[] = $indent . html_writer::link(
                $context->get_url(), $icon.$context->get_context_name());
        } else {
            $this->content->items[] = $indent . format_string(
                $context->get_context_name(), true, ['context' => $context]);
        }

        // If there are any child contexts, print them recursively.
        if (!empty($contexts[$contextid]->children)) {
            $level++;
            foreach ($contexts[$contextid]->children as $childcontextid) {
                $this->build_tree($childcontextid, $contexts, $systemcontext, $level);
            }
        }
    }

    protected function is_non_editing_teacher($context) {
        global $USER;

        $noneditingteachers = get_role_users($this->noneditingteacherroleid, $context);
        $noneditingteachersids = array_keys($noneditingteachers);
        return in_array($USER->id, $noneditingteachersids);
    }
}
