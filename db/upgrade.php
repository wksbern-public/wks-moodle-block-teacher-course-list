<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * WKS Moodle setup local plugin.
 *
 * @package    block_teacher_course_list
 * @copyright  2019 Liip AG {@link https://liip.ch}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * @param $oldversion
 * @return bool
 * @throws dml_exception
 * @throws downgrade_exception
 * @throws upgrade_exception
 */
function xmldb_block_wks_teacher_course_list_upgrade($oldversion) {
    if ($oldversion < 2019050100) {
        global $CFG;
        // Cleanup global setting that has been replaced by a plugin setting.
        if ($CFG->block_wks_teacher_course_list_visiblecategories) {
            unset_config('block_wks_teacher_course_list_visiblecategories');
        };
        // Assign savepoint reached.
        upgrade_block_savepoint(true, 2019050100, 'wks_teacher_course_list');
    }
    return true;
}
