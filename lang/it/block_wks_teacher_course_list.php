<?php
// This file is part of the blocks/wks_teacher_course_list Moodle plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Strings for component 'block_wks_teacher_course_list', language 'fr'
 *
 * @package   block_wks_teacher_course_list
 * @copyright 2017 Liip AG {@link https://liip.ch}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$string['pluginname'] = 'Campus: Teacher Course List';
$string['title'] = 'Teacher Course List';
$string['wks_teacher_course_list:addinstance'] = 'Add a new Teacher course list block';
$string['wks_teacher_course_list:viewblock'] = 'View a Teacher course list block';
