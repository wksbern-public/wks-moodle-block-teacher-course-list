<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * WKS teacher course block settings
 *
 * @package    block_wks_teacher_course_list
 * @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    // Categories that are visible to teachers.
    // Grundbildung/Fachschaftencategory 3.
    // Weiterbildung/Referenten category. 399.
    // Grundbildung/Lehrpersonencategory 401.
    $settings->add(new admin_setting_configtext(
        'block_wks_teacher_course_list/visiblecategories',
        new lang_string('visiblecategories', 'block_wks_teacher_course_list'),
        new lang_string('configvisiblecategories', 'block_wks_teacher_course_list'),
        '401,3,399', PARAM_SEQUENCE)
    );
}
